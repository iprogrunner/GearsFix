#pragma once
#include "controller.h"
#include "Plugin.h"

namespace console {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� MyForm
	/// </summary>
	public ref class FormView : public System::Windows::Forms::Form
	{
		bool repared;
		String^ image_path;
		IntPtr controller;
		ArrayList plugins;
	public:
		FormView(MyController &cntr)
		{
			InitializeComponent();
			repared = true;
			controller = static_cast<IntPtr>(&cntr);
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~FormView()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::TextBox^  textBox;
	protected:
	private: System::Windows::Forms::PictureBox^  pictureBox;
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem1;
	private: System::Windows::Forms::ToolStripMenuItem^  �����������ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  �����ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  ���������ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  ���������������ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  �������ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  ���������������ToolStripMenuItem;

	protected:

	private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->textBox = (gcnew System::Windows::Forms::TextBox());
			this->pictureBox = (gcnew System::Windows::Forms::PictureBox());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->toolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->�����������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->�����ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->���������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->���������������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->�������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->���������������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox))->BeginInit();
			this->menuStrip1->SuspendLayout();
			this->SuspendLayout();
			// 
			// textBox
			// 
			this->textBox->Location = System::Drawing::Point(401, 27);
			this->textBox->Multiline = true;
			this->textBox->Name = L"textBox";
			this->textBox->ReadOnly = true;
			this->textBox->ScrollBars = System::Windows::Forms::ScrollBars::Both;
			this->textBox->Size = System::Drawing::Size(359, 328);
			this->textBox->TabIndex = 0;
			// 
			// pictureBox
			// 
			this->pictureBox->Location = System::Drawing::Point(12, 27);
			this->pictureBox->Name = L"pictureBox";
			this->pictureBox->Size = System::Drawing::Size(383, 328);
			this->pictureBox->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox->TabIndex = 1;
			this->pictureBox->TabStop = false;
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
				this->toolStripMenuItem1,
					this->���������ToolStripMenuItem, this->�������ToolStripMenuItem
			});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(772, 24);
			this->menuStrip1->TabIndex = 2;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// toolStripMenuItem1
			// 
			this->toolStripMenuItem1->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->�����������ToolStripMenuItem,
					this->�����ToolStripMenuItem
			});
			this->toolStripMenuItem1->Name = L"toolStripMenuItem1";
			this->toolStripMenuItem1->Size = System::Drawing::Size(48, 20);
			this->toolStripMenuItem1->Text = L"����";
			// 
			// �����������ToolStripMenuItem
			// 
			this->�����������ToolStripMenuItem->Name = L"�����������ToolStripMenuItem";
			this->�����������ToolStripMenuItem->Size = System::Drawing::Size(153, 22);
			this->�����������ToolStripMenuItem->Text = L"������� ����";
			this->�����������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &FormView::�����������ToolStripMenuItem_Click);
			// 
			// �����ToolStripMenuItem
			// 
			this->�����ToolStripMenuItem->Name = L"�����ToolStripMenuItem";
			this->�����ToolStripMenuItem->Size = System::Drawing::Size(153, 22);
			this->�����ToolStripMenuItem->Text = L"�����";
			this->�����ToolStripMenuItem->Click += gcnew System::EventHandler(this, &FormView::�����ToolStripMenuItem_Click);
			// 
			// ���������ToolStripMenuItem
			// 
			this->���������ToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->���������������ToolStripMenuItem });
			this->���������ToolStripMenuItem->Name = L"���������ToolStripMenuItem";
			this->���������ToolStripMenuItem->Size = System::Drawing::Size(79, 20);
			this->���������ToolStripMenuItem->Text = L"���������";
			// 
			// ���������������ToolStripMenuItem
			// 
			this->���������������ToolStripMenuItem->Name = L"���������������ToolStripMenuItem";
			this->���������������ToolStripMenuItem->Size = System::Drawing::Size(173, 22);
			this->���������������ToolStripMenuItem->Text = L"���������������";
			this->���������������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &FormView::���������������ToolStripMenuItem_Click);
			// 
			// �������ToolStripMenuItem
			// 
			this->�������ToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->���������������ToolStripMenuItem });
			this->�������ToolStripMenuItem->Name = L"�������ToolStripMenuItem";
			this->�������ToolStripMenuItem->Size = System::Drawing::Size(69, 20);
			this->�������ToolStripMenuItem->Text = L"�������";
			// 
			// ���������������ToolStripMenuItem
			// 
			this->���������������ToolStripMenuItem->Name = L"���������������ToolStripMenuItem";
			this->���������������ToolStripMenuItem->Size = System::Drawing::Size(179, 22);
			this->���������������ToolStripMenuItem->Text = L"�������� �������";
			this->���������������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &FormView::���������������ToolStripMenuItem_Click);
			// 
			// FormView
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(772, 367);
			this->Controls->Add(this->pictureBox);
			this->Controls->Add(this->textBox);
			this->Controls->Add(this->menuStrip1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedDialog;
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"FormView";
			this->Text = L"FormView";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox))->EndInit();
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
	private: System::Void �����������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		OpenFileDialog^ openFileDialog1 = gcnew OpenFileDialog;
		openFileDialog1->Filter = "Bmp Files|*.bmp";
		if (openFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK)
		{
			image_path = openFileDialog1->FileName;
			pictureBox->Image = System::Drawing::Image::FromFile(image_path);
			repared = false;
		}
	}
private: System::Void �����ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
	this->Close();
}
private: System::Void ���������������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
	if (repared){
		MessageBox::Show("����������� �� ��������� � �������");
		return;
	}
	void *ptr = static_cast<void*>(controller);
	MyController *cntr = static_cast<MyController*>(ptr);
	vector<IObject*> object_array;
	vector<MyImage> images;
	textBox->Clear();
	using namespace Runtime::InteropServices;
    const char* chars = 
      (const char*)(Marshal::StringToHGlobalAnsi(image_path)).ToPointer();
    string path = chars;
	Marshal::FreeHGlobal(IntPtr((void*)chars));
	images.push_back(load_image((path.substr(0, path.find(".")) + "_1.bmp").c_str()));
	images.push_back(load_image((path.substr(0, path.find(".")) + "_2.bmp").c_str()));
	images.push_back(load_image((path.substr(0, path.find(".")) + "_3.bmp").c_str()));
	MyImage dst_image;
	int result_idx;
	tie(result_idx, object_array, dst_image) =
		cntr->repair_mechanism(load_image(path.c_str()), images);
	image_path = gcnew String((path.substr(0, path.find(".")) + "_out.bmp").c_str());
	save_image(dst_image, (path.substr(0, path.find(".")) + "_out.bmp").c_str());
	pictureBox->Image = System::Drawing::Image::FromFile
		(gcnew String((path.substr(0, path.find(".")) + "_out.bmp").c_str()));
	repared = true;
	textBox->Text += result_idx + "\r\n";
	textBox->Text += object_array.size() + "\r\n";
	for (size_t i = 0; i < object_array.size(); i++)
		textBox->Text += gcnew String(object_array[i]->ToString().c_str());
}
public: System::Void addText(string s){
	textBox->Text += gcnew String(s.c_str());
}
private: System::Void ���������������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
	Plugin plugin;
	PluginManager manager = plugin.dialog();
	vector<IImagePlugin*> plug = manager.GetUnaryPlugins();
	for (int i = 0; i < plug.size(); i++){
		String^ name = gcnew String(plug[i]->stringType());
		ToolStripItem^ item = gcnew ToolStripMenuItem();
		item->Name = name;
		item->Text = name;
		item->Click += gcnew EventHandler(this, &FormView::MenuItemClickHandler);
		this->�������ToolStripMenuItem->DropDownItems->Add(item);
		plugins.Add(static_cast<IntPtr>(plug[i]));
	}
	MessageBox::Show("������� ���������");
}

private: System::Void MenuItemClickHandler(Object^ sender, EventArgs^ e)
{
	ToolStripMenuItem^ clickedItem = (ToolStripMenuItem^)sender;
	if (String::IsNullOrEmpty(image_path)){
		MessageBox::Show("�������� �� �������");
		return;
	}
	using namespace Runtime::InteropServices;
	const char* chars =
		(const char*)(Marshal::StringToHGlobalAnsi(image_path)).ToPointer();
	string path = chars;
	Marshal::FreeHGlobal(IntPtr((void*)chars));
	IImagePlugin *plugin = NULL,*tmpPlug = NULL;
	void *ptr;
	for (int i = 0; i < plugins.Count; i++){
		ptr = static_cast<void*>((IntPtr)plugins[i]);
		tmpPlug = static_cast<IImagePlugin*>(ptr);
		if (!String::Compare(gcnew String(tmpPlug->stringType()), clickedItem->Name)){
			plugin = tmpPlug;
			break;
		}
	}
	if (plugin == NULL){
		MessageBox::Show("������������� ������ ��� �������. ����������, �������� �������");
		return;
	}
	MyImage img = load_image(path.c_str());
	plugin->filter(img);
	save_image(img, (path.substr(0, path.find(".")) + "_filtred.bmp").c_str());
	pictureBox->Image = System::Drawing::Image::FromFile
		(gcnew String((path.substr(0, path.find(".")) + "_filtred.bmp").c_str()));
	textBox->Text += gcnew String(plugin->stringType());
	textBox->Text += gcnew String(" was applied\r\n");
}
};
}
#pragma endregion