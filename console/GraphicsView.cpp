#include "GraphicsView.h"
#include <ctime>
#include < vcclr.h >

//extern "C"

GraphicsView::GraphicsView(){}

GraphicsView::~GraphicsView(){
	gcroot<FormView^> *pp = static_cast<gcroot<FormView^>*>(form_ptr);
	delete pp;
}

void GraphicsView::show(MyController controller){
	FormView form(controller);
	gcroot<FormView^> *pp = new gcroot<FormView^>(%form);
	form_ptr = static_cast<void*>(pp);
	Application::Run(%form);
}

void GraphicsView::OnFilterApplied(){
	time_t td;
	td = time(NULL);
	char c[26];
	ctime_s(c, 26, &td);
	string param(c);
	gcroot<FormView^> *pp = static_cast<gcroot<FormView^>*>(form_ptr);
	((FormView^)*pp)->addText(param + " Filter applied\r\n");
}

void GraphicsView::OnComponentsParsed(){
	time_t td;
	td = time(NULL);
	char c[26];
	ctime_s(c, 26, &td);
	string param(c);
	gcroot<FormView^> *pp = static_cast<gcroot<FormView^>*>(form_ptr);
	((FormView^)*pp)->addText(param + " Components Parsed\r\n");
}

void GraphicsView::OnDefectDetected(){
	time_t td;
	td = time(NULL);
	char c[26];
	ctime_s(c, 26, &td);
	string param(c);
	gcroot<FormView^> *pp = static_cast<gcroot<FormView^>*>(form_ptr);
	((FormView^)*pp)->addText(param + " Defect detected\r\n");
}

void GraphicsView::OnGearParamsDefined(){
	time_t td;
	td = time(NULL);
	char c[26];
	ctime_s(c, 26, &td);
	string param(c);
	gcroot<FormView^> *pp = static_cast<gcroot<FormView^>*>(form_ptr);
	((FormView^)*pp)->addText(param + " Number of gear`s tooth defined\r\n");
}

void GraphicsView::OnTaskComplete(){
	time_t td;
	td = time(NULL);
	char c[26];
	ctime_s(c, 26, &td);
	string param(c);
	gcroot<FormView^> *pp = static_cast<gcroot<FormView^>*>(form_ptr);
	((FormView^)*pp)->addText(param + " Task Complete\r\n");
}