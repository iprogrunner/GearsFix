#pragma once
#include "controller.h"
#include "Observer.h"
#include "FormView.h"

using namespace console;

class GraphicsView :
public Observer
{
	void* form_ptr;
public:
	GraphicsView();
	~GraphicsView();
	void show(MyController controller);
	void OnFilterApplied() override;
	void OnComponentsParsed() override;
	void OnDefectDetected() override;
	void OnGearParamsDefined() override;
	void OnTaskComplete() override;
};

