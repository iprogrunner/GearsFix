#pragma once
#include <windows.h>
#include "PluginManager.h"
#include "io.h"

class Plugin
{
public:
	PluginManager manager;
	Plugin();
	PluginManager dialog();
private:
	int findDLLs(const char *in_pluginsDir, vector<string> &out_file_names);
	int LoadDLLs(const char *in_pluginsDir, const vector<string> &in_file_names,
		HMODULE *out_pLib, vector<string> &out_lib_names);
	void LoadPlugins(HMODULE *in_pLib, const vector<string> &in_pLibNames);
};

