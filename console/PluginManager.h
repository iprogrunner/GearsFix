#pragma once
#include <vector>

#include "API.h"

using namespace std;

class PluginManager : public IPluginManager
{
	vector <IImagePlugin*> unPlugins;//IUnaryPlugin*
public:
	/************************************
	* setters. Inherited from superclass
	************************************/
	// registers unary plugins
	virtual void RegisterUnaryPlugin(IUnaryFactory* factory)
	{
		IImagePlugin* plugin = factory->Create();
		if (plugin)
			unPlugins.push_back(plugin);
	}

	/************************************
	* Getters
	************************************/
	vector<IImagePlugin*> &GetUnaryPlugins()
	{
		return unPlugins;
	}
};

