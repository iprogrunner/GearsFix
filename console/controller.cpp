#include <fstream>
#include <typeinfo>

#include "controller.h"
#include "transformation.h"

extern "C"

using std::tuple;

tuple<int, vector<IObject*>, MyImage> 
MyController::repair_mechanism(const MyImage& in, vector<MyImage> variants){
    // Base: return array of found objects and index of the correct gear - Status:Done
    // Bonus: return additional parameters of gears - Status:Done
    Matrix<int> binary;
    int result_idx = 0;
    auto object_array = vector<IObject*>();
    binary = model.binarization(in);
	model.filter(binary);
	model.classification(binary, object_array);
    IObject broken_object = model.detectBrakedown(object_array);
    tuple<int,Matrix<int>,tuple<int,int>> res = 
		model.choose(binary, variants, broken_object.location);
    result_idx = get<0>(res);
	MyImage res_Image =
		model.setGear(binary, broken_object.location, get<1>(res), get<2>(res));
    return make_tuple(result_idx, object_array, res_Image);    
}