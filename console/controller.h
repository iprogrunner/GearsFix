#pragma once

#include <tuple>
#include <vector>
#include <memory>

#include "MyObject.h"
#include "io.h"
#include "transformation.h"

using namespace std;

class MyController{
    public:
		Transformation model;
		MyController(const Transformation &apmodel) :model(apmodel){};
		tuple<int, vector<IObject*>, MyImage>
			repair_mechanism(const MyImage& in, vector<MyImage> variants);
};