#pragma once

#include "matrix.h"
#include "EasyBMP.h"

#include <tuple>

typedef Matrix<std::tuple<uint, uint, uint>> Image;
typedef Image MyImage;

MyImage load_image(const char*);
void save_image(const MyImage&, const char*);
