#include "GraphicsView.h"

[STAThread]
void Main()
{
	Transformation model;
    GraphicsView view;
	model.addObserver(view);
	MyController controller(model);
	view.show(controller);
}
