#include <vector>
#include <algorithm>
#include <cmath>
#include <cfloat>

#include "transformation.h"
#include "myObject.h"
#include "io.h"

#define M_PI 3.14159265359

extern "C"

using namespace std;

Matrix<int> Transformation::binarization(const MyImage &im){
    double threshold = 30.0;
    Matrix<int> res(im.n_rows,im.n_cols);
    for (uint i=0;i<im.n_rows;i++)
        for (uint j = 0;j<im.n_cols;j++){
            tuple<uint, uint, uint> pnt = im(i,j);
            if ((0.299*get<0>(pnt)+0.587*get<1>(pnt)+0.114*get<2>(pnt))>threshold)
                res(i,j) = 1;
            else res(i,j) = 0;
        }
    return res;
}

void Transformation::filter(Matrix<int> &arr){
    for (uint i=0;i<arr.n_rows;i++)
        for (uint j = 0;j<arr.n_cols;j++){
            uint prow = (i==0)?0:i-1;
            uint pcol = (j==0)?0:j-1;
            uint rows = ((i==arr.n_rows-1)||(i==0))?2:3;
            uint cols = ((j==arr.n_cols-1)||(j==0))?2:3;
            Matrix<int> surround(arr.submatrix(prow,pcol,rows,cols));
            vector<int> surr;
            for (uint k=0;k<surround.n_rows;k++)
                for (uint l = 0;l<surround.n_cols;l++) surr.push_back(surround(k,l));
            sort(surr.begin(),surr.end());
            arr(i,j) = surr[(surr.size()-1)/2];
        }
	this->FilterApplied();
}
   
void Transformation::fill(Matrix<int> &matrix, vector<Component> &elements){
    int index = 2;
	Component *comp = new Component(index);
	if (matrix(0,0) == 1){
		comp->addPoint(0,0);
		matrix(0,0) = index;
	}
	for (uint j = 1; j < matrix.n_cols; j++)
		if (matrix(0,j) == 1){
			if (matrix(0,j-1)==0){
				elements.push_back(*comp);
				index++;
				comp = new Component(index);
				comp->addPoint(j,0);
				matrix(0,j) = index;
			}
			else{
				comp->addPoint(j,0);
				matrix(0,j) = index;
			}
		}
	for (uint i = 1; i < matrix.n_rows; i++)
		if (matrix(i,0) == 1){
			if (matrix(i-1,0)==0){
				elements.push_back(*comp);
				index++;
				comp = new Component(index);
				comp->addPoint(0,i);
				matrix(i,0) = index;
			}
			else{
				comp->addPoint(0,i);
				matrix(i,0) = index;
			}
		}
	for (uint i=1; i<matrix.n_rows; i++)
		for (uint j=1; j<matrix.n_cols;j++)
			if (matrix(i,j) == 1){
				int left = matrix(i,j-1);
				int up = matrix(i-1,j);
				if (left == 0) index++;
				if ((left==0)&&(up==0)){
					elements.push_back(*comp);
					comp = new Component(index);
					comp->addPoint(j,i);
					matrix(i,j) = index;
				}
				else{
					elements.push_back(*comp);
					for (size_t k=0;k<elements.size();k++)
						if (find(elements[k].indexes.begin(),elements[k].indexes.end(), 
							(left==0)?up:left)!= elements[k].indexes.end()){
							comp = new Component(elements[k]);
							elements.erase(elements.begin()+k);
							break;
						}
					comp->addPoint(j,i);
					if (left == 0) comp->indexes.push_back(index);
					matrix(i,j) = index;
					if ((left!=0)&&(up!=0)&&(find(comp->indexes.begin(),comp->indexes.end(),up) ==
						comp->indexes.end()))
						for (size_t k=0;k<elements.size();k++)
							if (find(elements[k].indexes.begin(),elements[k].indexes.end(),up)!= 
								elements[k].indexes.end()){
								comp->join(elements[k]);
								elements.erase(elements.begin()+k);
								break;
							}
				}
			}
	elements.push_back(*comp);
	this->ComponentsParsed();
}

//http://cs.brown.edu/~pff/papers/dt-final.pdf
Matrix<double> Transformation::findCenter(const Matrix<int> &matrix){
	Matrix<double> distance(matrix.n_rows,matrix.n_cols);
	int k =0;
	vector<int> v;
	vector<double> z;
	double s = 0;
	for (uint j=0; j<matrix.n_cols;j++){
		v.clear();
		z.clear();
		v.resize(matrix.n_rows+1,0);
		z.resize(matrix.n_rows+1,0.0);
		k = 0;
		v[0] = 0;
		z[0] = -DBL_MAX;
		z[1] = DBL_MAX;
        for (uint i=1; i<matrix.n_rows; i++){
			s = double(((matrix(i,j)==0?0:15000.23)+i*i-
				((matrix(v[k],j)==0?0:15000.23)+v[k]*v[k])))/(2*i-2*v[k]);
			while (s <= z[k]){
				k--;
				s = double(((matrix(i,j)==0?0:15000.23)+i*i-
					((matrix(v[k],j)==0?0:15000.23)+v[k]*v[k]))/(2*i-2*v[k]));
			}
			k++;
			v[k] = i;
			z[k] = s;
			z[k+1] = DBL_MAX;
        }
		k = 0;
		int rows = matrix.n_rows;
		for (int i=0; i < rows; i++){
			while (z[k+1]<i) k++;
			distance(i,j) = (i-v[k])*(i-v[k])+(matrix(v[k],j)==0?0:15000.23);
		}
	}
	return distance;
}

Matrix<double> Transformation::findCenterReverse(const Matrix<double> &matrix){
	Matrix<double> distance(matrix.n_rows,matrix.n_cols);
	int k =0;
	vector<int> v;
	vector<double> z;
	double s = 0;
	for (uint i=0; i<matrix.n_rows;i++){
		v.clear();
		z.clear();
		v.resize(matrix.n_cols+1,0);
		z.resize(matrix.n_cols+1,0.0);
		k = 0;
		v[0] = 0;
		z[0] = -DBL_MAX;
		z[1] = DBL_MAX;
        for (uint j=1; j<matrix.n_cols; j++){
			s = double((matrix(i,j)+j*j-(matrix(i,v[k])+v[k]*v[k])))/(2*j-2*v[k]);
			while (s <= z[k]){
				k--;
				s = double((matrix(i,j)+j*j-(matrix(i,v[k])+v[k]*v[k])))/(2*j-2*v[k]);
			}
			k++;
			v[k] = j;
			z[k] = s;
			z[k+1] = DBL_MAX;
        }
		k = 0;
		int cols = matrix.n_cols;
		for (int j=0; j < cols; j++){
			while (z[k+1]<j) k++;
			distance(i,j) = (j-v[k])*(j-v[k])+ matrix(i,v[k]);
		}
	}
	return distance;
}
 
void Transformation::deleteIndexes(Matrix<int> &matrix, const vector<int> &indexes){
	for (uint i=0; i<matrix.n_rows; i++)
		for (uint j=0; j<matrix.n_cols;j++)
			if ((matrix(i,j) != 0)&&
				(find(indexes.begin(),indexes.end(),matrix(i,j))!=indexes.end())) matrix(i,j) = 0;
} 
 
double Transformation::bigRadius(const Matrix<int> &matrix, tuple<int,int> center, 
const vector<int> &indexes){
	double res = 0;
	for (uint i=0; i<matrix.n_rows; i++)
		for (uint j=0; j<matrix.n_cols;j++)
			if ((matrix(i,j) != 0)&&(find(indexes.begin(),indexes.end(),matrix(i,j))!=indexes.end())
				&&((matrix(i-1,j) == 0)||(matrix(i+1,j) == 0)||
				(matrix(i,j-1) == 0)||(matrix(i,j+1) == 0))){
				double val = sqrt((get<0>(center)-j)*(get<0>(center)-j)+
					(get<1>(center)-i)*(get<1>(center)-i));
				if (val > res) res = val;
			}
	return res;
} 
 
int Transformation::gearNum(const Matrix<int> &matrix, tuple<int,int> center, double radius){
	int res = 0;
	bool gear = false;
	int x = 0;
	int y = 0;
	for (int i=0; i<360; i++){
		x = get<0>(center) + (radius+2)*cos(i*2*M_PI/360);
		y = get<1>(center) + (radius+2)*sin(i*2*M_PI/360);
		if ((matrix(y,x)==0)&&gear){ 
			res++;
			gear = false;
		}
		if ((matrix(y,x)!=0)&&(!gear)) gear = true;
	}
	return res;
} 
 
void Transformation::classification(Matrix<int> &matrix, 
vector<IObject*> &objList){
	vector<Component> elements;
    fill(matrix,elements);
    Matrix<double> distance = findCenter(matrix);
	distance = findCenterReverse(distance);
	for (uint i=1; i<matrix.n_rows; i++)
		for (uint j=1; j<matrix.n_cols;j++)
			if (matrix(i,j)!=0)
				for (auto it=elements.begin();it!=elements.end();it++)
					if (find(it->indexes.begin(),it->indexes.end(),matrix(i,j))!=it->indexes.end())
						if (distance(i,j)>it->radius){
							it->radius = distance(i,j);
							it->center = make_tuple(j,i);
						}
	vector<int> deleteList;
	for (auto it=elements.begin();it!=elements.end();it++){
        if (it->weight == 0) continue;
		int massCentX = it->xSum/it->weight;
		int massCentY = it->ySum/it->weight;
		if ((it->weight >= 300)&&(it->weight<=1700)){
			Axis *asix = new Axis(it->center);
			objList.push_back(asix);
            deleteList.insert(deleteList.end(),it->indexes.begin(),it->indexes.end());
		}
		else if (it->weight>1700){
            bool isBroken = sqrt((get<0>(it->center)-massCentX)*(get<0>(it->center)-massCentX) + 
            (get<1>(it->center)-massCentY)*(get<1>(it->center)-massCentY)) > 2.0;
			Gear *gear = new Gear(it->center,sqrt(it->radius),bigRadius(matrix,it->center,
				it->indexes),isBroken,gearNum(matrix,it->center,sqrt(it->radius)));
			objList.push_back(gear);
            if (isBroken) deleteList.insert(deleteList.end(),it->indexes.begin(),it->indexes.end());
		}
		else deleteList.insert(deleteList.end(),it->indexes.begin(),it->indexes.end());
	}
	deleteIndexes(matrix,deleteList);
	this->GearParamsDefined();
}

bool Transformation::tryPaste(const Matrix<int> &to, tuple<int,int> toCenter, 
    const Matrix<int> &from, tuple<int,int> fromCenter){
    for (uint i = 0;i<from.n_rows;i++)
        for (uint j = 0;j<from.n_cols;j++){
            if (from(i,j)!=0&&((i==0||from(i-1,j)==0)||(j==0||from(i,j-1)==0)||
                (i==from.n_rows-1||from(i+1,j)==0)||(j==from.n_cols-1||from(i,j+1)==0))){
                int x = get<0>(toCenter)+j-get<0>(fromCenter);
                int y = get<1>(toCenter)+i-get<1>(fromCenter);
                int cols = to.n_cols;
                int rows = to.n_rows;
                if ((x<0)||(y<0)||(x>=cols)||(y>=rows)||(to(y,x)!=0)){
                    return false;
                }
            }    
        }
    return true;
}

IObject Transformation::detectBrakedown(vector<IObject*> &object_array){
	for (size_t i = 0; i<object_array.size(); i++)
		if (typeid(*object_array[i]) == typeid(Axis)){
			this->DefectDetected();
			return *object_array[i];
		}
		else if (typeid(*object_array[i]) == typeid(Gear)){
			IObject *obj = object_array[i];
			if (static_cast<Gear*>(obj)->is_broken){
				this->DefectDetected();
				return *obj;
			}
		}

}

tuple<int,Matrix<int>,tuple<int,int>> Transformation::choose(const Matrix<int> &matrix, 
	vector<MyImage> &images, tuple<int, int> center){
    int res = 0;
    int max_weight =0;
    Matrix<int> tmp_matrix;
    Matrix<int> res_matrix;
    tuple<int,int> res_center;
    vector<Component> elements;
    for (size_t i = 0;i<images.size();i++){
        tmp_matrix = binarization(images[i]);
        filter(tmp_matrix);
        elements.clear();
        fill(tmp_matrix,elements);
        Component gear = elements[elements.size()-1];
        if (gear.weight > max_weight){
            tuple<int,int> gCenter = make_tuple(gear.xSum/gear.weight,gear.ySum/gear.weight);
            if (tryPaste(matrix,center,tmp_matrix,gCenter)){
                res_matrix = tmp_matrix.deep_copy();
                res = i+1;
                max_weight = gear.weight;
                res_center = gCenter;
            }
        }
    }
    return make_tuple(res,res_matrix,res_center);
}
   
MyImage Transformation::setGear(Matrix<int> &to, tuple<int, int> toCenter, const Matrix<int> &from,
        tuple<int,int> fromCenter){
    for (uint i = 0;i<from.n_rows;i++)
        for (uint j = 0;j<from.n_cols;j++){
            if (from(i,j)!=0){
                int x = get<0>(toCenter)+j-get<0>(fromCenter);
                int y = get<1>(toCenter)+i-get<1>(fromCenter);
                to(y,x) = from(i,j);
            }
        }
	MyImage res(to.n_rows, to.n_cols);
    for (uint i = 0;i<to.n_rows;i++)
        for (uint j = 0;j<to.n_cols;j++){
            if (to(i,j)!=0) res(i,j) = make_tuple(0,255,0);
            else res(i,j) = make_tuple(0,0,0);
        }
	this->TaskComplete();
    return res;
}