#pragma once

#include <vector>

#include "matrix.h"
#include "io.h"
#include "observable.h"
#include "MyObject.h"

class Transformation: public Observable{
    public:
		Matrix<int> binarization(const MyImage &im);
    void filter(Matrix<int> &array);
    void classification(Matrix<int> &matrix, vector<IObject*> &objList);
	IObject detectBrakedown(vector<IObject*> &object_array);
    tuple<int,Matrix<int>,tuple<int,int>> choose(const Matrix<int> &matrix, 
		vector<MyImage> &images, tuple<int, int> center);
	MyImage setGear(Matrix<int> &to, tuple<int, int> toCenter, const Matrix<int> &from,
        tuple<int,int> fromCenter);
    private:
	struct Component{
		Component(int ind):indexes(),xSum(0),ySum(0),weight(0),center(0,0),radius(0){indexes.push_back(ind);};
		Component(const Component &comp):indexes(comp.indexes),xSum(comp.xSum),ySum(comp.ySum),
            weight(comp.weight),center(comp.center),radius(comp.radius){};
		vector<int> indexes;
		long xSum;
		long ySum;
		int weight;
        tuple<int,int> center;
		double radius;
		
		void join(const Component &comp){
			for (size_t i = 0; i<comp.indexes.size(); i++)
				indexes.push_back(comp.indexes[i]);
			xSum += comp.xSum;
			ySum += comp.ySum;
			weight += comp.weight;
		}
		
		void addPoint(int x,int y){
			xSum += x;
			ySum += y;
			weight++;
		}
		
		void out(){
			cout << "<";
			for (size_t k=0;k<indexes.size();k++) cout<<indexes[k]<<",";
			cout << endl << xSum << " " << ySum << " " << weight << ">"<<endl;
		}
	};
	
	static int gearNum(const Matrix<int> &matrix, tuple<int,int> center, double radius);
	static double bigRadius(const Matrix<int> &matrix, tuple<int,int> center, const vector<int> &indexes);
	static void deleteIndexes(Matrix<int> &matrix, const vector<int> &indexes);
	void fill(Matrix<int> &matrix,vector<Component> &elements);
    static Matrix<double> findCenter(const Matrix<int> &matrix);
	static Matrix<double> findCenterReverse(const Matrix<double> &matrix);
    static bool tryPaste (const Matrix<int> &to, tuple<int,int> toCenter, const Matrix<int> &from, 
        tuple<int,int> fromCenter);
};