#include "matrix.h"
#include "io.h"

//abstract plugin
class IPlugin
{
public:
  // returns string name of the plugin
	virtual char* stringType() = 0;
};

// abstract plugin to apply unary operation
class IImagePlugin: public IPlugin
{
public:
  // applies unary operation and returns result to the first parameter
	virtual void filter(Image &img) = 0;
};

// abstract factory
template<typename Interface> class IFactory 
{
public:
	virtual Interface* Create() = 0;
};

// abstract factory to create unary and binary plugins
typedef IFactory<IImagePlugin> IUnaryFactory;

// abstract plugin manager
class IPluginManager 
{
public:
  // registers plugins
	virtual void RegisterUnaryPlugin(IUnaryFactory* factory) = 0;
};



// type of an entry point
typedef void (*regFuncType)(IPluginManager *);