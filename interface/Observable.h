#pragma once

#include <list>
#include <iterator>
#include "Observer.h"

using namespace std;

class Observable{
    list<Observer*> observers;

    public:
    Observable():observers(){}
    virtual ~Observable(){observers.clear();}
    
    void addObserver(Observer &o){observers.push_back(&o);}
    //void removeObserver(Observer o){observers.remove(o);}
    
    protected:
    virtual void FilterApplied(){
        for (auto it = observers.begin();it != observers.end();it++)
            (*it)->OnFilterApplied();
    }
    
    virtual void ComponentsParsed(){
        for (auto it = observers.begin();it != observers.end();it++)
			(*it)->OnComponentsParsed();
    }
    
    virtual void DefectDetected(){
        for (auto it = observers.begin();it != observers.end();it++)
			(*it)->OnDefectDetected();
    }
    
    virtual void GearParamsDefined(){
        for (auto it = observers.begin();it != observers.end();it++)
			(*it)->OnGearParamsDefined();
    }
    
    virtual void TaskComplete(){
        for (auto it = observers.begin();it != observers.end();it++)
			(*it)->OnTaskComplete();
    }
};