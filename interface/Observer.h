#pragma once

class Observer{
    public:
    Observer(){};
    virtual ~Observer(){};
    virtual void OnFilterApplied(){};
    virtual void OnComponentsParsed(){};
    virtual void OnDefectDetected(){};
    virtual void OnGearParamsDefined(){};
    virtual void OnTaskComplete(){};
};