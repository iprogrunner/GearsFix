#include <fstream>
#include <vector>
#include <tuple>
#include <ctime>
#include <conio.h>

#include "consoleView.h"
#include "io.h"
#include "MyObject.h"
#include "Plugin.h"

using namespace std;

ConsoleView::ConsoleView(){}

ConsoleView::~ConsoleView(){}

int ConsoleView::show(int argc, char **argv, Controller &controller){
	if (argc == 5 && !strcmp(argv[4], "-filter")){
		filter = true;
	}
	else if (argc != 4){
        cout << "Usage: " << endl << argv[0]
             << " <in_image.bmp> <out_image.bmp> <out_result.txt>" << endl;
		cout << "Press enter to exit" << endl;
		while (!kbhit()){};
        return 0;
    }
	else filter = false;

    try {
        Image src_image = load_image(argv[1]);
        ofstream fout(argv[3]);
		
        vector<Image> images;
        string path(argv[1]);
        images.push_back(load_image((path.substr(0,path.find("."))+"_1.bmp").c_str()));
        images.push_back(load_image((path.substr(0,path.find("."))+"_2.bmp").c_str()));
        images.push_back(load_image((path.substr(0,path.find("."))+"_3.bmp").c_str()));
		
		if (filter){
			Plugin plugin;
			plugin.dialog(src_image);
		}

        vector<IObject*> object_array;
        Image dst_image;
        int result_idx;
        tie(result_idx, object_array, dst_image) = 
			controller.repair_mechanism(src_image,images);
        save_image(dst_image, argv[2]);

        fout << result_idx << endl;
        fout << object_array.size() << endl;
        for (size_t i = 0; i < object_array.size(); i++)
            object_array[i]->Write(fout);
		cout << "Press enter to exit" << endl;
		while (!kbhit()){};
        return 0;
    } 
    catch (const string &s) {
        cerr << "Error: " << s << endl;
		cout << "Press enter to exit" << endl;
		while (!kbhit()){};
        return 1;
    }
}

void ConsoleView::OnFilterApplied(){
	if (filter){
		time_t td;
		td = time(NULL);
		char c[26];
		ctime_s(c, 26, &td);
		cout << c << " Filter applied" << endl;
	}
}

void ConsoleView::OnComponentsParsed(){
	if (filter){
		time_t td;
		td = time(NULL);
		char c[26];
		ctime_s(c, 26, &td);
		cout << c << " Components parsed" << endl;
	}
}

void ConsoleView::OnDefectDetected(){
	if (filter){
		time_t td;
		td = time(NULL);
		char c[26];
		ctime_s(c, 26, &td);
		cout << c << " Defect detected" << endl;
	}
}

void ConsoleView::OnGearParamsDefined(){
	if (filter){
		time_t td;
		td = time(NULL);
		char c[26];
		ctime_s(c, 26, &td);
		cout << c << " Number of gear`s tooth defined" << endl;
	}
}

void ConsoleView::OnTaskComplete(){
	if (filter){
		time_t td;
		td = time(NULL);
		char c[26];
		ctime_s(c, 26, &td);
		cout << c << " Task complete" << endl;
	}
}