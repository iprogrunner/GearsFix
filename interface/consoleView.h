#pragma once

#include "controller.h"
#include "Observer.h"

class ConsoleView: public Observer{
	bool filter;
    public:
    ConsoleView();
    ~ConsoleView();
    int show(int argc, char **argv, Controller &controller);
    void OnFilterApplied() override;
    void OnComponentsParsed() override;
    void OnDefectDetected() override;
    void OnGearParamsDefined() override;
    void OnTaskComplete() override;
};