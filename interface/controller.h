#include <tuple>
#include <vector>
#include <memory>

#include "MyObject.h"
#include "io.h"
#include "transformation.h"

using namespace std;

class Controller{
    public:
		Transformation model;
		Controller(const Transformation &apmodel) :model(apmodel){};
		tuple<int, vector<IObject*>, Image>
		repair_mechanism(const Image& in, vector<Image> variants);
};