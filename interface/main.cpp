#include "consoleView.h"

int main(int argc, char **argv)
{
	Transformation model;
    ConsoleView console;
	model.addObserver(console);
	Controller controller(model);
	return console.show(argc, argv, controller);
}
